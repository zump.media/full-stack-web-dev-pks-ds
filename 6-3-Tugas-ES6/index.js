//soal 1
//jawaban soal 1

let  luasPersegipanjang = function(p,l)
{
	return p*l;
};
console.log('Function =',luasPersegipanjang(10,5));



//soal 2
//jawaban soal 2

let  luas = (p,l) => { return p*l;};
console.log('Arrow =',luas(10,5));


const newFunction = function literal(firstName, lastName){
    return {
      fullName: function(){
        console.log(firstName + " " + lastName)
      }
    }
}
   
  //Driver Code 
  newFunction("William", "Imoh").fullName() 
  
//soal 3
//jawaban soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
};

const {firstName, lastName, address, hobby} =newObject

// Driver code
console.log(firstName, lastName, address, hobby)


//soal 4
//jawaban soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combined =[west,east]
//Driver Code
console.log(combined)

//soal 5
//jawaban soal 5
const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 

console.log(before)