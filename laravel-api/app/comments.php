<?php

namespace App;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class comments extends Model
{
    protected $table = 'comments';
    protected $fillable = ['id','content','post_id','user_id'];
    protected $keyType = 'string';
    public $incrementing = false;
    protected static function boot()
    {
        parent::boot();
        static::creating(function($model){
            if(empty($model->id) ){
               $model->id= Str::uuid();
            }

            $model->user_id = auth()->id;
        });
    }

    public function post() 
    {
         return $this->hasMany('App\post');
    }

    public function user() 
    {
         return $this->belongTO('App\Users');
    }
}
