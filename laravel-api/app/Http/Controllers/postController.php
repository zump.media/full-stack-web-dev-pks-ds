<?php

namespace App\Http\Controllers;
use App\post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
class postController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get data from table post
         $post = post::latest()->get();

         //make response JSON
         return response()->json([
             'success' => true,
             'message' => 'List Data post',
             'data'    => $post  
         ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * @param  mixed $request
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'title'   => 'required',
            'description' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $post = post::auth()->user();

        //save to database
        $post = post::create([
            'title'     => $request->title,
            'description'   => $request->description,
            'user_id' => $user->id
        ]);

        //success save to database
        if($post) {

            return response()->json([
                'success' => true,
                'message' => 'post Created',
                'data'    => $post  
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'post Failed to Save',
        ], 409);
    }

    /**
     * Display the specified resource.
     * @param  mixed $id
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //find post by ID
        $post = post::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data post',
            'data'    => $post 
        ], 200);
    }

   

    /**
     * Update the specified resource in storage.
     * @param  mixed $request
     * @param  mixed $post
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'title'   => 'required',
            'description' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $post = post::findOrFail($post->id);

        if($post) {
            $user = auth()->user();
            if($post->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'data post bukan milik anda',
                    'data'    => $post  
                ]);
            }
            //update post
            $post->update([
                'title'     => $request->title,
                'description'   => $request->description
            ]);

            return response()->json([
                'success' => true,
                'message' => 'post Updated',
                'data'    => $post  
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'post Not Found',
        ], 404);

    }

    /**
     * Remove the specified resource from storage.
     * @param  mixed $id
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //find post by ID
        $post = post::findOrfail($id);

        if($post) {
            
            $user = auth()->user();
            if($post->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'data post bukan milik anda',
                    'data'    => $post  
                ]);
            }
            //delete post
            $post->delete();

            return response()->json([
                'success' => true,
                'message' => 'post Deleted',
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'post Not Found',
        ], 404);
    }
}
