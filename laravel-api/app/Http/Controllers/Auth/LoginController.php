<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allReques = $request->all();
        $validator = Validator::make($allReques, [
         
             
             'email' => 'required',
             'password' => 'required'
             
         ]);
 
         if ($validator->fails()) {
 
             return response()->json($validator->errors(),400);
         }

         $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json([

                'success'=> false,
                'message'=> 'Email atau Pasword tidak di temukan',

            ], 401);
        }
        return response()->json([

            'success'=> true,
            'message'=> 'USer berhasil login',
            'data' => [
                'user'=> auth()->user(),
                'token'=>$token
            ]

        ], 401);
        

    }
}
