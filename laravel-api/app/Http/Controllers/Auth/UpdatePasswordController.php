<?php

namespace App\Http\Controllers\Auth;

use App\Users;
use App\User;
use App\OtpCode;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UpdatePasswordController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allReques = $request->all();
        $validator = Validator::make($allReques, [
         
             
             'email' => 'required',
             'password' => 'required|confirmed|min:6'
             
         ]);
 
         if ($validator->fails()) {
 
             return response()->json($validator->errors(),400);
         }

         $user = User::where('email', $request->email)->first();
         if (!user)
         {
            return response()->json([
                'success'=> true,
                'message'=> 'Email tidak di temukan',
             ], 400);
         }

         $user->update([
             'password' => Hash::make($request->password)
         ]);

         return response()->json([
            'success'=> true,
            'message'=> 'PAssword berhasil di rubah',
            'data' => $user
         ]);

 
    }
}
