<?php

namespace App\Http\Controllers\Auth;

use App\Users;
use App\User;
use App\OtpCode;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
class RegenerateOtpCodeController extends Controller
{
    /**
     * Handle the incoming   request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allReques = $request->all();
       $validator = Validator::make($allReques, [
        
            
            'email' => 'required',
            
        ]);

        if ($validator->fails()) {

            return response()->json($validator->errors(),400);
        }

        $user = User::where('email', $request->email)->first();

        $user->otp_code()->delete();

        do {

            $random = mt_rand( 100000 , 999999 );
            $check = OtpCode::where('otp', $random)->first();

        } while ($check);

        $otp_code = OtpCode::create([

            'otp' => $random,
            'valid_until' => $now->addMinutes(5),
            'user_id' => $user->id

        ]);

        return response()->json([
             'success'=> true,
             'message'=> 'Data user telah di buat',
             'data' => [
                
                    'user' => $user,
                    'otp_code' => $otp_code
                ]
            
            ]);


    }
}
