<?php

namespace App;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class post extends Model
{
    protected $table = 'posts';
    protected $fillable = ['id','title','description','user_id'];
    protected $keyType = 'string';
    public $incrementing = false;
    protected static function boot()
    {
        parent::boot();
        static::creating(function($model){
            if(empty($model->id) ){
               $model->id= Str::uuid();
            }

            $model->user_id = auth()->id;
        });
    }

    public function comments() 
    {
         return $this->hasMany('App\comments','post_id', 'id');
    }

    public function user() 
    {
         return $this->belongTO('App\Users');
    }
}
