<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
class User extends Authenticatable implements JWTSubject
{

   
   protected $keyType = 'string';
   public $incrementing = false;
   protected static function boot()
   {
       parent::boot();
       static::creating(function($model){
        if(empty($model->id)){
            $model->id= Str::uuid();
        }
       });
   }
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'nama', 'role_id' , 'password' ,'email_verified_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected static function boot()

    {
        parent::boot();

     static::creating(function ($model) {
        if (empty($model->getKeyName())){
            $model->{$model->getKeyName()}=Str::uuid();
        }

        $model->role_id = Role::where('name', 'author')->first()->id;
     });
    }
    public function role() 
    {
         return $this->belongsTo('App\role');
    }
    public function otp_code() 
    {
         return $this->hasOne('App\OtpCode');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function post() 
    {
         return $this->hasMany('App\post');
    }

    public function comment() 
    {
         return $this->belongTO('App\comment');
    }
}
