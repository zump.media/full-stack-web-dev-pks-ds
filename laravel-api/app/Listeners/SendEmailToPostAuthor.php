<?php

namespace App\Listeners;
use App\Mail\PostAuhorMail;
use illiminate\Support\Facades\Mail;
use App\Events\CommentStoredEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendEmailToPostAuthor implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CommentStoredEvent  $event
     * @return void
     */
    public function handle(CommentStoredEvent $event)
    {
        Mail::to($event->comments->posts->user->email)->send(new PostAuthorMail($event->comments));
    }
}
